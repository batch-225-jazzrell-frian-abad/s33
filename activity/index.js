


fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  })

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {
    const title = json.title;
    const completed = json.completed;
    console.log(json)
    console.log(`The item "${title}" on the list has a status of ${completed}`);
  })



fetch('https://jsonplaceholder.typicode.com/todos', {

	method: 'POST',
	
	headers: {
		'Content-type' : 'application/json'
	},
	
	body: JSON.stringify({
		title: 'Created To do List Item',
		completed: false,
		userId: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Updated To Do List Item',
	  	dateCompleted: 'Pending',
	  	status: 'Pending',
	  	description: "To update the my to do list with a different data structure",
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({	
	  	dateCompleted: '07/09/21',
	  	status: 'Completed',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));
